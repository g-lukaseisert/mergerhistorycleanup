#!/bin/bash

# IllustrisTNG
SUITE=IllustrisTNG
SIMULATION=$1
SIMDIR=~/IllustrisTNG/${SIMULATION}
SNAPNUM_FIRST=0
SNAPNUM_LAST=99

BASEDIR=${SIMDIR}/output
TREEDIR=${SIMDIR}/postprocessing/trees/SubLink_gal
WRITEDIR=./output/${SIMULATION}/postprocessing/MergerHistory

# Make sure that directory exists
if [ ! -d $WRITEDIR ]; then
  mkdir -p $WRITEDIR
fi

module load intel/19.1.3
module load hdf5-serial
export LD_LIBRARY_PATH=$HDF5_HOME/lib

WRITEPATH=$WRITEDIR/merger_history_supplementary_cleaned
./sublink/Examples/merger_history_supplementary_cleaned $SUITE $BASEDIR $TREEDIR $WRITEPATH $SNAPNUM_FIRST $SNAPNUM_LAST
