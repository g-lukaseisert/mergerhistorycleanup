#!/bin/bash

# IllustrisTNG
SIMULATION=$1
SIMDIR=~/IllustrisTNG/${SIMULATION}
SNAPNUM_FIRST=0
SNAPNUM_LAST=99

BASEDIR=${SIMDIR}/output
TREEDIR=${SIMDIR}/postprocessing/trees/SubLink_gal
WRITEDIR=./output/${SIMULATION}/trees/SubLink_gal

# Make sure that directory exists
if [ ! -d $WRITEDIR ]; then
  mkdir -p $WRITEDIR
fi

module load anaconda/3

python3 ./sublink/Python/create_columns.py $BASEDIR $TREEDIR $WRITEDIR $SNAPNUM_FIRST $SNAPNUM_LAST
