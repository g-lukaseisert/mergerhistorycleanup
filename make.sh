#!/bin/bash

module load intel/19.1.3 
module load hdf5-serial
export LD_LIBRARY_PATH=$HDF5_HOME/lib

cd ./sublink/Examples/
make
