# MergerHistoryCleanup

Clean up the Merger History Catalogues
Changes with respect to the original algorithm:

- Ignore subhalos with subhalo_flag = 0
- Ignore secondaries whose lifetimes were <= 2 snapshots excluding the next progenitor itself (Implemented by checking the snapshot of the MainLeafProgenitor) 

Additional change which has been switched off in the current version but is still in the code
(- Ignore secondaries with less than 50 stellar particles at all previous snapshot (Loop along the secondarys main progenitor branch))

These changes are implemented in the sublink/Examples/merger_history_cleaned(/bonus/supplementary).cpp

To compile and run the code:
1. fix path in InputOutput/ReadTreeHDF5.hpp Line 627 accordingly (ending /column)
2. bash make.sh to compile the code 
3. bash run_all.sh to calculate the merger catalogues 

Changes which were introduce relative to the original sublink code: (compare to the initial commits)
- Trivially sublink/Examples
- Add SubhaloFlag/MassLenType to InputOutput/ReadTreeHDF5.hpp 
(Note that I hardcoded the path to the output of create_subhalo_flag.sh in line 627)
- Add new types to Util/TreeTypes.hpp
- Modify Python/create_columns.py such that it reads and writes a single sublink field using the /tree_extended files (originally tree) 
Furthermore I enforce the dtype to be np.int = long. So this script is now highly specialiced for this purpose
Best chance to avoid this sloppy changes would be to add the SubhaloFlag directly to the /tree_extended files which is missing atm