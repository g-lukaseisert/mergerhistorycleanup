#!/bin/bash

for SIMULATION in 'TNG50-1'
do

    echo $SIMULATION
    #bash read_subhalo_flag.sh $SIMULATION
    bash merger_history_cleaned.sh $SIMULATION
    bash merger_history_bonus_cleaned.sh $SIMULATION
    bash merger_history_supplementary_cleaned.sh $SIMULATION

done